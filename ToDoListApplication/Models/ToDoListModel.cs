﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToDoListApplication.Models
{
    public class ToDoListModel
    {
        public List<ToDoListItem> items;

        public ToDoListModel()
        {
            items = new List<ToDoListItem>();
        }
    }

    public class ToDoListItem
    {
        public int id;
        public string content;
        public string imageName;
        public bool isCompleted;
        public bool isDeleted;
        public byte[] Image { get; set; }


    }


}