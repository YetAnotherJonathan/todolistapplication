﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToDoListApplication.Services;

namespace ToDoListApplication.Controllers
{
    public class HomeController : Controller
    {   
        private static ToDoListService _service; 
        public ActionResult Index()
        {
            _service = ToDoListService.GetService();
            return View(_service.getModel());
        }
        public ActionResult SoftDelete(int id)
        {
            var item = _service.getModel().items.SingleOrDefault(x => x.id == id);
            item.isDeleted = true;

            return RedirectToAction("Index");
        }
        public ActionResult ToggleCompleteness(int id)
        {
            var item = _service.getModel().items.SingleOrDefault(x => x.id == id);

            item.isCompleted = !item.isCompleted;

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult AddItem(string message, HttpPostedFileBase file)
        {            
            if(_service == null)
            {
                _service = ToDoListService.GetService();
            }

            int nextId;
            nextId = _service.getModel().items.Count();
            
            byte[] data = null;
            string fileName = null;
            if (file != null && file.ContentLength > 0)
            {
                MemoryStream target = new MemoryStream();
                file.InputStream.CopyTo(target);
                data = target.ToArray();

                fileName = file.FileName;
            }

            _service.getModel().items.Add(
                new Models.ToDoListItem() {
                 id = nextId,
                 content = message,
                 Image = data,
                 imageName = fileName
                });
            return RedirectToAction("Index", _service.getModel());
        }

        public ActionResult RetrieveImage(int id)
        {
            if(_service == null)
            {
                _service = ToDoListService.GetService();
            }
            var item = _service.getModel().items.SingleOrDefault(x => x.id == id);

            return File(item.Image, "image/jpg");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}