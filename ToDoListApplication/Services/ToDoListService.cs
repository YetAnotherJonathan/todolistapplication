﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ToDoListApplication.Models;
namespace ToDoListApplication.Services
{
    public class ToDoListService
    {
        private static ToDoListService _instance;
        private ToDoListModel model;

        public static ToDoListService GetService()
        {
            if(_instance == null)
            {
                _instance = new ToDoListService();
            }

            return _instance;
        }

        private ToDoListService()
        {
            model = new ToDoListModel();
        }

        public ToDoListModel getModel()
        {
            return model;
        }

    }
}